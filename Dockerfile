FROM debian:buster-slim
RUN DEBIAN_FRONTEND=noninteractive apt-get update; apt-get install -y \
	opensmtpd \
	ca-certificates
ADD smtpd.conf /etc/smtpd.conf
RUN echo email > /etc/mailname
ENTRYPOINT ["/usr/sbin/smtpd"]
CMD ["-d", "-v", "-Tall"]
